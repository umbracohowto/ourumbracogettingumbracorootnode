﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using System.Linq;
using Umbraco.Cms.Web.Common;
using Umbraco.Cms.Web.Common.Controllers;

namespace GettingUmbracoRootNode
{
	public class SampleController : UmbracoPageController
	{
		private readonly UmbracoHelper _umbracoHelper; // https://our.umbraco.com/documentation/reference/querying/umbracohelper/
		public SampleController(
			UmbracoHelper umbracoHelper, 
			ILogger<UmbracoPageController> logger,
			ICompositeViewEngine compositeViewEngine
			) : base(logger, compositeViewEngine)
		{
			_umbracoHelper = umbracoHelper;
		}

		[Route("[controller]/[action]/{rootNodeId?}")]
		[HttpGet]
		public ActionResult Index(int? rootNodeId = null)
		{
			var rootNode = _umbracoHelper.ContentAtRoot().FirstOrDefault();
			return Content($"Root nodeID is {rootNodeId ?? rootNode?.Id}");
		}
	}
}
